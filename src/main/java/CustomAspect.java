import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class CustomAspect {
    @Around(value = "@annotation(annotation)")
    public String TryCatch(final ProceedingJoinPoint joinPoint, final CatchNPE annotation) throws Throwable {
        try {
            System.out.println("before");
            final String retVal = (String) joinPoint.proceed();
            return retVal;
        } catch (NullPointerException e) {
            System.err.println("Call to " + joinPoint.getSignature() + " throw " + e.toString());
//            System.err.println(Arrays.toString(e.getStackTrace()));
            return null;
        } finally {
            System.out.println("after");
        }
    }
}